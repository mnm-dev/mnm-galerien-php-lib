<?php

class GalleryHelper {
    
   public static function getGalleryXmlPath() {
      global $globalLogger;
      $originalRequest=$_SESSION["REQUEST_URI"];
            $globalLogger->info("originalRequest returns '$originalRequest'");

      // remove query string
      $originalRequest = strtok($originalRequest,'?');
      
      $originalRequest=preg_replace("/\/gallery.php$/", "", $originalRequest);
      $originalRequest=preg_replace("/\/index.php$/", "", $originalRequest);
      $originalRequest=preg_replace("/\/index.html$/", "", $originalRequest);
      $originalRequest=preg_replace("/\/gallery.xml$/", "", $originalRequest);
      $originalRequest=preg_replace("/\/$/", "", $originalRequest);


      $globalLogger->info("originalRequest after cleanup '$originalRequest'");
      $realPath = realpath(SERVER_BASE_DIR . $originalRequest . "/gallery.xml");

      if (!$realPath) {
         error_log ("real path returned '" . $realPath . "'", 0);
         error_log ("originalRequest is : '" . $originalRequest . "'", 0);
         error_log ("_SESSION[REQUEST_URI] was : '" . $_SESSION["REQUEST_URI"] . "'", 0);
         error_log ("session id was : '" . session_id() . "'", 0);
         header("Location: /");
         exit(0);
      }
      $globalLogger->info("getGalleryXmlPath returns '$realPath'");
      return $realPath;
   }

   public static function readGalleryXmlPath() {
      readfile(GalleryHelper::getGalleryXmlPath());
   }

}

?>