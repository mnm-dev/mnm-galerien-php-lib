
var googleMap;
var pictureLocations=new Array();
var resolution=900;


/**
 * taken from http://jquery-howto.blogspot.com/2009/09/get-url-parameters-values-with-jquery.html
 */
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

var initializeGallery=
         function()
         {
            $("#sideBar").show();
            
            if (getUrlVars()["resolution"]) {
               resolution=getUrlVars()["resolution"];
            }
         
            var galleryXml={};
            var $thumbsList=$("#thumbsList");
         
            var shadow = new google.maps.MarkerImage(cdnPrefixGeneral + '/images/pictureShadow.png',
                  new google.maps.Size(41, 13),
                  new google.maps.Point(0,0),
                  new google.maps.Point(0, 0));
            
            var leftPositionHidden = -1
                  * ($("#sideBar").outerWidth() + parseInt($("#sideBar").css("margin-left"), 10) + parseInt(
                        $("body").css("margin-left"), 10));


            var minLat=null;
            var maxLat=null;
            var minLong=null;
            var maxLong=null;
            
            function getCdnImageUrl($imageNode, subPath, extension) {
               $file=$imageNode.find("file");
               return cdnPrefixPhotos +  $file.attr("fileNameNoExt").substr(0, 4) + "/" + subPath + "/" + $file.attr("hash") + extension;
            }
            
            function getImageUrl($imageNode) {
               return getCdnImageUrl($imageNode, resolution, ".jpg");
            }
            function getThumbUrl($imageNode) {
               return getCdnImageUrl($imageNode, "thumbs", ".jpg");
            }
            function getIconUrl($imageNode) {
               return getCdnImageUrl($imageNode, "icons", ".png");
            }

            function getPictureLocation($galleryImageNode) {
               tmpText=""+$galleryImageNode.find("coordinates").text();
               if (tmpText=="") { 
                  return null; 
               }
               pictureLocation={};
               pictureLocation.lat=parseFloat(tmpText.split(",")[0]);
               pictureLocation.long=parseFloat(tmpText.split(",")[1]);

               if (!minLat || minLat>pictureLocation.lat) minLat=pictureLocation.lat;
               if (!maxLat || maxLat<pictureLocation.lat) maxLat=pictureLocation.lat;
               if (!minLong || minLong>pictureLocation.long) minLong=pictureLocation.long;
               if (!maxLong || maxLong<pictureLocation.long) maxLong=pictureLocation.long;
                              
               pictureLocation.fileNameNoExt=$galleryImageNode.find('file').attr("fileNameNoExt");
               return pictureLocation;
            }
            
            function loadImage($galleryImageNode) {
               var $imageDiv=$("#imagePage");
               var $imageTag=$("#mainImage");
               var dateTime=$galleryImageNode.find('info').attr("dateTime").substr(0,16);
               $imageTag.data("data-galleryImageNode", $galleryImageNode);
               
               $dimensions=$galleryImageNode.find('dimension[maxLength="' + resolution + '"]');
               
               $imageTag.attr("width",$dimensions.attr("width")+"px");
               $imageTag.attr("height",$dimensions.attr("height")+"px");
               $imageTag.attr("src", cdnPrefixGeneral + "/images/1p.gif");
               $(".imageDescription", $imageDiv).text($galleryImageNode.find("caption").text());
               
               $(".dateTime", $imageDiv).text(dateTime);
               $(".filename", $imageDiv).text($galleryImageNode.find('file').attr("fileNameNoExt"));

               var tmpText="";
               $galleryImageNode.find("keyword").each( function(){tmpText = tmpText + ((tmpText.length==0)?"":", ") + $(this).text();});
               $(".keywords", $imageDiv).text(tmpText);
               tmpText="";
               $galleryImageNode.find("person").each( function(){tmpText = tmpText + ((tmpText.length==0)?"":", ") + $(this).text();});
               $(".peopleInfo", $imageDiv).text(tmpText);
               tmpText="";
               $galleryImageNode.find("place").each( function(){tmpText = tmpText + ((tmpText.length==0)?"":", ") + $(this).text();});
               $(".imageTitle", $imageDiv).text(dateTime + " " + tmpText);

               tmpText=$galleryImageNode.find("pictureCameraDetails").text();
               $(".pictureCameraDetails", $imageDiv).text(tmpText);               
               
               tmpText=""||$galleryImageNode.find("coordinates").text();
               if (tmpText=="") {
                  $("#mapIcon").hide();
               } else {
                  $("#mapIcon").show();
                  $(".coordinates", $imageDiv).text(tmpText);
               }
               $imageTag.attr("src", getImageUrl($galleryImageNode));
            }

            
            function getCurrentGalleryImageNode() {
               return  $("#mainImage").data("data-galleryImageNode");
            }
            
            function markSelectedThumbnailAsSelected($thumbImg) {
               if (! $thumbImg) {
                  // if we did not pass the current $thumbImg we pick it via the currently shown image
                  $thumbImg = $('img[src="' + getThumbUrl(getCurrentGalleryImageNode()) + '"]', $thumbsList);
               }
               
               $("img.selectedThumbnail", $thumbsList).removeClass("selectedThumbnail");
               $thumbImg.addClass("selectedThumbnail");
               $thumbsList.scrollTo($thumbImg);
               // window.scrollTo(0, $thumbImg.position().top - 31);
            }

            function createThumbNailListItem(imageXmlNode) {
               var $galleryImageNode=imageXmlNode;
               var $thumbInfo = $galleryImageNode.find('thumb');
               var text='<li><img src="' + getThumbUrl($galleryImageNode) + '"'
                        + ' height="' + $thumbInfo.attr("height") + '"'
                        + ' width="' + $thumbInfo.attr("width") + '"'
                        + ' data-imageid="' +  $galleryImageNode.find('file').attr('fileNameNoExt') + '"'
                        + ' "><div>' + $thumbInfo.attr("thumbtitle") + '</div></li>';
               
               pictureLocation=getPictureLocation($galleryImageNode);
               if (pictureLocation!=null) {
                  pictureLocations.push(pictureLocation);
               }

               $thumbsList.append(text);
            }
            
                    
            function createImageList(xml) {
               var haveSelectedImage=false;
               $thumbsList.empty();
               var preselectedFileNameWithOutExtension=getUrlVars()["file"];
               $(xml).find('image').each( function(){
                     createThumbNailListItem($(this));
                     if (! haveSelectedImage) {
                           if (preselectedFileNameWithOutExtension) {
                              var currentFileNameWithOutExtension=$(this).find('file').attr('fileNameNoExt');
                              if (currentFileNameWithOutExtension==preselectedFileNameWithOutExtension){
                                 loadImage($(this));
                                 haveSelectedImage=true;
                              }
                           } else {
                              // pick the first
                              loadImage($(this));
                              haveSelectedImage=true;
                           }
                     }
                  });
            }
            
            function setMenuAreas()
            {
               var heightTopMenu = parseInt($("#topImageNavigation").css("height"), 10);
               var height = parseInt($("#mainImage").css("height"), 10) - heightTopMenu;
               var imageWidth = parseInt($("#mainImage").css("width"), 10);
               var leftWidth = imageWidth / 4;
               var left = $("#mainImage").position().left;
            
               $("#topImageNavigation").css("left", left);
               $("#topImageNavigation").css("width", imageWidth);
            
               $("#leftImageNavigation").css("top", heightTopMenu);
               $("#leftImageNavigation").css("height", height);
               $("#leftImageNavigation").css("width", leftWidth);
               $("#leftImageNavigation").css("left", left);
               
            }


            function loadImageMarkThumbSelectedAndPrepareMenues($galleryImageNode, $optionalThumbNailImage) {
               loadImage($galleryImageNode);
               markSelectedThumbnailAsSelected($optionalThumbNailImage);
               setMenuAreas();
            }
            
            function findImageNodeForFilenameNoExt(fileNameNoExt) {
               return $(galleryXml).find('image file[fileNameNoExt="' + fileNameNoExt + '"]').parent();
            }
            
            
            function selectAndDisplayNextImageFromThumbNail() {
               var $thumbImage = $("img", $(this));
               var $currentGalleryImageNode=findImageNodeForFilenameNoExt($thumbImage.attr("data-imageid"));
               loadImageMarkThumbSelectedAndPrepareMenues($currentGalleryImageNode, $thumbImage);
            }            

            function selectAndDisplayNextImageFromCurrentImage() {
               var $nextGalleryImageNode=getCurrentGalleryImageNode().next();
               if (! $nextGalleryImageNode || ! $nextGalleryImageNode.is("image")) {
                  $nextGalleryImageNode = $(galleryXml).find('image:first');
               }
               loadImageMarkThumbSelectedAndPrepareMenues($nextGalleryImageNode);
            }
            
            function selectAndDisplayPreviousImageFromCurrentImage() {
               var $previousGalleryImageNode=getCurrentGalleryImageNode().prev();
               if (! $previousGalleryImageNode || ! $previousGalleryImageNode.is("image")) {
                  $previousGalleryImageNode = $(galleryXml).find('image:last');
               }
               loadImageMarkThumbSelectedAndPrepareMenues($previousGalleryImageNode);
            }

            function selectAndDisplayImageFromFileNameWithOutExtension(fileNameNoExt) {
               var $currentGalleryImageNode=findImageNodeForFilenameNoExt(fileNameNoExt);
               loadImageMarkThumbSelectedAndPrepareMenues($currentGalleryImageNode);
            }
            
            
            function onSuccessFullGalleryXmlLoading(xml) {
               
               galleryXml=xml;
               
               var $albumNode=$(galleryXml).find("album")

               createImageList(xml);
               markSelectedThumbnailAsSelected();
               setMenuAreas();
               

               $(document).keydown(keyDownHandler);
               
               $("li", $thumbsList).click(selectAndDisplayNextImageFromThumbNail);
               
               $("#leftImageNavigation").click(selectAndDisplayPreviousImageFromCurrentImage);
               
               $("#mainImage").click(selectAndDisplayNextImageFromCurrentImage);

               $("#noJavascript").hide();
               
               if (pictureLocations && pictureLocations.length>0) {
                  $("#pictureMapMenu").show();
                  $(".pictureMap").click(openGoogleMapAllPics);
               }

               $("title").text ($(galleryXml).find("collections>collection>title").text() + " - MnM - " + $albumNode.attr("date"));
               
            }

            function keyDownHandler(event) {
                if (  event.keyCode == '32'
                   || event.keyCode == '39' ) {
                  event.preventDefault();
                  selectAndDisplayNextImageFromCurrentImage();
                } else if (event.keyCode == '8'
                      || event.keyCode == '37' ) {
                   event.preventDefault();
                   selectAndDisplayPreviousImageFromCurrentImage();
                }
            }

            function showTopNav() {
               $("#topImageNavigationMenu") .show();
               $("#topImageNavigationMenu").stop(true, true).animate(
                     {
                        height : "100%",
                        opacity : 0.8
                     },
                     {
                        duration : 200
                     });
            }
            function hideTopNav() {
               $("#topImageNavigationMenu").hide();
               $("#topImageNavigationMenu").stop(true, true).animate(
                     {
                         height: 0
                        ,opacity : 0
                     },
                     {
                        duration : 200
                     });
            }
            $("#sideBarHandle, #sideBarHide").click(function()
            {
               var newLeftSideBar = 0;
               var newLeftSideBarHandle = leftPositionHidden;                     
               var newOpacitySideBar = 1;
               var newOpacitySideBarHandle = 0;
               if (parseInt($("#sideBar").css("left"),10) == 0)
               {
                  newLeftSideBar = leftPositionHidden;
                  newLeftSideBarHandle = 0;
                  newOpacitySideBar = 0;
                  newOpacitySideBarHandle = 1;
               }
               $("#sideBar").animate(
               {
                  left : ("" + newLeftSideBar + "px"),
                  opacity : newOpacitySideBar
               },
               {
                  duration : 300
               });
               $("#sideBarHandle").animate(
               {
                  left : ("" + newLeftSideBarHandle + "px"),
                  opacity : newOpacitySideBarHandle
               },
               {
                  duration : 300
               });
            });

            function getCurrentLatLng(){
               var picturePoint=null;
               $currentGalleryImageNode = getCurrentGalleryImageNode();
               if ($currentGalleryImageNode!=null) {
                  pictureLocation = getPictureLocation($currentGalleryImageNode);
                  
                  if (pictureLocation==null) {
                     if (pictureLocations!=null && pictureLocation.length>0) {
                        // try to get the middle picture from the gallery, it might be a good centre
                        var midPos = Math.round((pictureLocation.length-1)/2);
                        pictureLocation = pictureLocations[midPos];
                     }
                  }   
                  
                  if (pictureLocation!=null) {
                     picturePoint = new google.maps.LatLng(pictureLocation.lat, pictureLocation.long);
                  }   
               }
               return picturePoint;
            }

            function openGoogleMap() {
               
               var picturePoint = getCurrentLatLng();
               var myOptions = {
                 zoom: 10,
                 center: picturePoint,
                 mapTypeId: google.maps.MapTypeId.TERRAIN
               };

               $("#googleMapDialog").dialog({ modal: true, width:'630px'});

               googleMap = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
               
               new google.maps.Marker({
                   position: picturePoint,
                   map: googleMap
               });
            }
            
            function openGoogleMapAllPics() {
               
               var centrePoint = null;
               var latLongBounds = null;
               if (minLat && maxLat && minLong && maxLong) {
                  latLongBounds = new google.maps.LatLngBounds(new google.maps.LatLng(minLat, minLong),new google.maps.LatLng(maxLat, maxLong));
                  centrePoint = new google.maps.LatLng((minLat+maxLat)/2, (minLong+maxLong)/2);
               } else {
                  centrePoint= new getCurrentLatLng();
               }
                  
               var myOptions = {
                     zoom: 10,
                     center: centrePoint,
                     mapTypeId: google.maps.MapTypeId.TERRAIN
               };

               $("#googleMapDialog").dialog({ modal: true, show: 'slide', width:'630px'});
               googleMap = new google.maps.Map(document.getElementById("map_canvas"), myOptions);                     

               if (latLongBounds) {
                  googleMap.fitBounds(latLongBounds);
               }
               
               for (var i = 0; i < pictureLocations.length; i++) {
                  var pictureLocation = pictureLocations[i];
                  var myLatLng = new google.maps.LatLng(pictureLocation.lat, pictureLocation.long);
                  var $imageNode=findImageNodeForFilenameNoExt(pictureLocation.fileNameNoExt);                 
                  var image = new google.maps.MarkerImage( getIconUrl($imageNode),
                                      new google.maps.Size(32, 22),
                                      new google.maps.Point(0,0),
                                      new google.maps.Point(0, 0));
                  var marker = new google.maps.Marker({
                      position: myLatLng,
                      map: googleMap,
                      shadow: shadow,
                      icon: image,
                      title: pictureLocation.fileNameNoExt,
                      zIndex: i
                  });
                 google.maps.event.addListener(marker, 'click', function() {
                    selectAndDisplayImageFromFileNameWithOutExtension(this.getTitle());
                    $("#googleMapDialog").dialog( "close" );
                 });
                }

            }
            

            var galleryXmlText=document.getElementById("galleryXmlContainer").textContent;
            var parsedGalleryXml=new DOMParser().parseFromString(galleryXmlText, "application/xml");
            onSuccessFullGalleryXmlLoading(parsedGalleryXml);
            
            $(".googleMapLink").click(openGoogleMap);
            
            $("#sideBarHandle").click();
            $("#homeIcon").click(function(){window.location.href="/";});
            
            $("#help").click( function(){$("#helpDialog").dialog({ modal: true, width:'500px' });} );
            $("#topImageNavigation").hover( showTopNav, hideTopNav );
         }

$(document).ready(initializeGallery);
