# Why Another Gallery ##

[Over the years][1] we created a custom gallery work-flow that suits us quite well.

While it is specific to our needs, it has a few general aspects that will work for others

  - JavaScript uses meta data to generate view (inform of a gallery specific gallery.xml file)
  - CDN/S3 hosted images
  - PHP is used to secure (oauth2) as well as minor path configuration, this is now part of a [separate library][2]

## Setup ##

   - Setup the auth module
   - Copy config/example-gallery-config.php to config/galerien-config.php and fix up the paths
   - Copy config/log4php-config-example.xml to config/log4php-config.xml and fix the log file path
   - Make sure the rewrite rule in the example .htaccess is working 
   - Test 


  [1]: http://mnm.at/blog/tag/gallery  "Gallery History"
  [2]: https://bitbucket.org/mnm-dev/simple-php-oauth2-authentication-library