<?php
class CloudFrontSignatureUtils {
   private static $URL_UNSAFE_B64_CHARS = array ('+','=','/'
   );
   private static $URL_SAFE_B64_CHARS = array ('-','_','~'
   );


   public static function rsa_sha1_sign($policy, $private_key_filename) {
      $signature = "";
      openssl_sign ( $policy, $signature, file_get_contents ( $private_key_filename ) );
      return $signature;
   }


   public static function url_safe_base64_encode($value) {
      $encoded = base64_encode ( $value );
      return str_replace ( CloudFrontSignatureUtils::$URL_UNSAFE_B64_CHARS,
            CloudFrontSignatureUtils::$URL_SAFE_B64_CHARS, $encoded );
   }


   public static function url_safe_base64_decode($encoded) {
      $decoded = base64_decode (
            str_replace ( CloudFrontSignatureUtils::$URL_SAFE_B64_CHARS,
                  CloudFrontSignatureUtils::$URL_UNSAFE_B64_CHARS, $encoded ) );
      return $decoded;
   }


   public static function getSignedPolicy($private_key_filename, $policy) {
      $signature = CloudFrontSignatureUtils::rsa_sha1_sign ( $policy, $private_key_filename );
      $encoded_signature = CloudFrontSignatureUtils::url_safe_base64_encode ( $signature );
      return $encoded_signature;
   }


   public static function getNowPlus2HoursInUTC() {
      $dt = new DateTime ( 'now', new DateTimeZone ( 'UTC' ) );
      $dt->add ( new DateInterval ( 'P1D' ) );
      return $dt->format ( 'U' );
   }


   public static function pkcs7_pad($data, $size) {
      $length = $size - strlen ( $data ) % $size;
      return $data . str_repeat ( chr ( $length ), $length );
   }


   public static function pkcs7_unpad($data) {
      return substr ( $data, 0, - ord ( $data [strlen ( $data ) - 1] ) );
   }


   public static function encryptForUrl($value) {
      global $globalLogger;
      $iv = openssl_random_pseudo_bytes ( 16, $strong );
      $encryptedValue = openssl_encrypt ( CloudFrontSignatureUtils::pkcs7_pad ( $value, 16 ), 'AES-256-CBC',
            ENCRYPTION_KEY, 0, $iv );
      $encryptedUrlValue = CloudFrontSignatureUtils::url_safe_base64_encode ( $iv ) . CloudFrontSignatureUtils::url_safe_base64_encode (
            $encryptedValue );
      $globalLogger->debug($encryptedUrlValue);
      return $encryptedUrlValue;
   }


   public static function decryptFromUrl($encryptedUrlValue) {
      $ivPart = strtok ( $encryptedUrlValue, "__" ) . "__";
      $valuePart = strtok ( "__" ) . "__";
      $iv = CloudFrontSignatureUtils::url_safe_base64_decode ( $ivPart );
      $encryptedValue = CloudFrontSignatureUtils::url_safe_base64_decode ( $valuePart );
      $decryptedValue = CloudFrontSignatureUtils::pkcs7_unpad (
            openssl_decrypt ( $encryptedValue, 'AES-256-CBC', ENCRYPTION_KEY, 0, $iv ) );
      return $decryptedValue;
   }


   public static function setCookie($name, $val, $domain) {
      // using our own implementation because
      // using php setcookie means the values are URL encoded and then AWS CF fails
      header ( "Set-Cookie: $name=$val; path=/; domain=$domain; secure; httponly", false );
   }


   public static function getSessionIdUrlString() {
      global $globalLogger;
      $globalLogger->debug("getSessionIdUrlString");
      return "?v=" . CloudFrontSignatureUtils::encryptForUrl ( session_id () );
   }
}
?>