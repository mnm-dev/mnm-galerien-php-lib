<?php
require_once( dirname( __FILE__ ) . "/config/gallery-config.php");
require_once( AUTH_LIB_DIR . "/setup.php");
require_once (GALERIEN_DIR . '/CloudFrontSignatureUtils.php');

class CloudFrontSignedCookieHelper {


   public static function setCloudFrontCookies() {
      $cloudFrontHost = CDN_HOST_PHOTOS;
      $cloudFrontCookieExpiry = CloudFrontSignatureUtils::getNowPlus2HoursInUTC ();

      $customPolicy = '{"Statement":[{"Resource":"https://' . $cloudFrontHost .
             '/*","Condition":{"DateLessThan":{"AWS:EpochTime":' . $cloudFrontCookieExpiry . '}}}]}';

      $encodedCustomPolicy = CloudFrontSignatureUtils::url_safe_base64_encode ( $customPolicy );

      $customPolicySignature = CloudFrontSignatureUtils::getSignedPolicy ( CLOUDFRONT_KEY_PATH, $customPolicy );

      CloudFrontSignatureUtils::setCookie ( "CloudFront-Policy", $encodedCustomPolicy, $cloudFrontHost );
      CloudFrontSignatureUtils::setCookie ( "CloudFront-Signature", $customPolicySignature, $cloudFrontHost );
      CloudFrontSignatureUtils::setCookie ( "CloudFront-Key-Pair-Id", CLOUDFRONT_KEY_PAIR_ID, $cloudFrontHost );
   }


   public static function setCookieIfAuthenticatedOrRedirect() {
      global $globalLogger;
      if (! isset ( $_GET ["v"] ) || $_GET ["v"] === null || $_GET ["v"] == "") {
         redirectToHomePage ();
      }

      $sessionId = CloudFrontSignatureUtils::decryptFromUrl ( $_GET ["v"] );
      $referer=isset ($_SERVER["HTTP_REFERER"])?$_SERVER["HTTP_REFERER"]:"NOT SET";
      $globalLogger->debug ( "setCookieIfAuthenticatedOrRedirect got '$sessionId' HTTP_REFERER : '$referer'");
      session_id ( $sessionId );
      session_start ();

      if (! AuthHelper::isAuthenticated ()) {
         redirectToHomePage ();
      }

      $globalLogger->debug ( "setting CF cookies");
      CloudFrontSignedCookieHelper::setCloudFrontCookies ();
   }
}

if (strpos ( $_SERVER ["REQUEST_URI"], "CloudFrontSignedCookieHelper.php" ) > 0) {
   CloudFrontSignedCookieHelper::setCookieIfAuthenticatedOrRedirect ();
}
?>