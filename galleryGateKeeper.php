<?php
require_once( dirname( __FILE__ ) . "/config/gallery-config.php");
require_once( AUTH_LIB_DIR . "/setup.php");
AuthHelper::sessionStart ();

require_once (GALERIEN_DIR . '/GalleryHelper.php');
require_once (GALERIEN_DIR . '/CloudFrontSignatureUtils.php');

$globalLogger->debug("start index.php gatekeeper");

// see comments in required file before changing this
require_once (SERVER_AUTH_DIR . '/isAuthorized.php');

$generalServerUrl=CDN_PREFIX_GENERAL . GALERIEN_CONTEXT;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<title>MnM gallery</title>
<script type="text/javascript" src="<?php echo CDN_PREFIX_PHOTOS; ?>galerien_lib/CloudFrontSignedCookieHelper.php<?php echo CloudFrontSignatureUtils::getSessionIdUrlString();?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $generalServerUrl; ?>/css/gallery.V4.0.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $generalServerUrl; ?>/css/3rdParty/ui-darkness/jquery-ui-1.8.16.custom.css"/>
<script type="text/javascript">
<!--
var cdnPrefixGeneral='<?php echo $generalServerUrl; ?>';
var cdnPrefixPhotos='<?php echo CDN_PREFIX_PHOTOS ?>';
//-->
</script>
<script id="galleryXmlContainer" type="application/xml">
<?php

GalleryHelper::readGalleryXmlPath();

?>
</script>
<script type="text/javascript" src="<?php echo $generalServerUrl; ?>/js/3rdParty/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="<?php echo $generalServerUrl; ?>/js/3rdParty/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $generalServerUrl; ?>/js/3rdParty/jquery.scrollTo-1.4.2.min.js"></script>
<script type="text/javascript" src="<?php echo $generalServerUrl; ?>/js/gallery.V4.1.js"></script>
<script type="text/javascript" src="//maps.google.com/maps/api/js"></script>
</head>
<body>
    <div id="noJavascript" style="position:fixed">
      Your browser is not able to render this page.
      Please <a style="text-decoration: underline; font-weight: bold;" href="indexOld.php">use our old gallery style</a>
      <script type="text/javascript">$("#noJavascript").hide();</script>
    </div>
   <div id="sideBarHandle">
      <div>T h u m b n a i l s</div>
   </div>
   <div id="sideBar" style="display: none">
      <div id="sideBarHide">H i d e &nbsp; T h u m b s</div>
      <ul id="thumbsList"><li></li></ul>
   </div>
   <div class="menu" id="leftmenu">
      <ul>
         <li id="homeIcon"><a title="To home page" href="/">&nbsp;&nbsp;&nbsp;</a></li>
         <li id="directoryUp"><a title="To the parent directory" href="./../index.php">&nbsp;&nbsp;&nbsp;</a></li>
         <li id="pictureMapMenu" class="pictureMap" style="display: none;"
             title="Show a map with the locations of the pictures of this gallery">M</li>
         <li id="help" title="Show some guidance" >?</li>
      </ul>
      <div id="helpDialog" style="display: none;" title="Kind of a Help">
         <ul>
            <li>You can click on the right side of the picture to go to the next image (or rather the right  three quarters)</li>
            <li>You can click on the left quarter of the picture to go to the previous image</li>
            <li>&lt;space&gt; or &lt;right&gt; move to the next picture</li>
            <li>&lt;backspace&gt; or &lt;left&gt; move to the previous picture</li>
            <li>The top of the picture has more information and potentially a Google Map link</li>
            <li>Optionally, depending on the availability of GPS information, the left menu displays an "M".'<br />
                The "M" menu shows a google map with all the images represented by little picture icons, 
                you can select a picture from the map, by clicking on it.</li>
         </ul>
      </div>
   </div>
   <div id="imagePage">
      <div id="topImageNavigation" class="menu">
         <div id="topImageNavigationMenu" class="menu">
            <a class="googleMapLink"><img id="mapIcon" src="<?php echo $generalServerUrl; ?>/images/icons/mapIcon2.png" alt="a map icon" /></a>
            <ul id="additionalImageInformation">
               <li>Filename: <span class="filename"></span> </li>
               <li>Date/Time: <span class="dateTime"></span></li>
               <li>People: <span class="peopleInfo"></span></li>
               <li>Keywords: <span class="keywords"></span></li>
               <li>Coordinates: <span class="coordinates"></span></li>
               <li>Camera details: <span class="pictureCameraDetails"></span></li>
            </ul>
         </div>
      </div>
      <div id="googleMapDialog" style="display: none; 630px" title="Picture Point">
        <div id="map_canvas" style="width:600px;height:400px"></div>
      </div>
      <div class="menu" id="leftImageNavigation"></div>
      <img id="mainImage" class="noselect" src="<?php echo $generalServerUrl; ?>/images/1p.gif" alt="The main image"/>
      <h3 class="imageTitle"></h3>
      <p class="imageDescription"></p>
      <p class="peopleInfo"></p>
      <p class="keywords"></p>
   </div>
</body>
</html>
