<?php

// where ever you have put this module
define ( "GALERIEN_CONTEXT", "galleryLib" );
define ( "GALERIEN_DIR", $_SERVER ['DOCUMENT_ROOT'] . "/" . GALERIEN_CONTEXT );

// where ever you set up https://bitbucket.org/mnm-dev/simple-php-oauth2-authentication-library
define ( "AUTH_LIB_DIR", $_SERVER ['DOCUMENT_ROOT'] . "/auth" );

/*
 * the CDN prefixes have to have trailing slashes
 */
define ( "CDN_PREFIX_GENERAL", "//example.com/" );
define ( "CDN_PREFIX_PHOTOS", "//example.com/gallery-images/" );

?>
